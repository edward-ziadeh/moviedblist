package com.example.mymac.moviedblist;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.mymac.moviedblist.Model.Item;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;



public class MovieRcyclerAdapter extends RecyclerView.Adapter<MovieRcyclerAdapter.Holder> {

    ArrayList<Item> items;
    Context context;

    public class Holder  extends RecyclerView.ViewHolder{
        ImageView imageView;
        TextView title, year,type;
        public Holder(@NonNull View itemView) {
            super(itemView);
            title= itemView.findViewById(R.id.movie_row_title);
            year= itemView.findViewById(R.id.movie_row_year);
            type= itemView.findViewById(R.id.movie_row_type);
            imageView= itemView.findViewById(R.id.movie_row_image);
        }
    }

    public MovieRcyclerAdapter(ArrayList<Item> items, Context context){
        this.items= items;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_row,parent,false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        holder.title.setText("" +items.get(position).getTitle());
        holder.year.setText("year: "+ items.get(position).getYear());
        holder.type.setText("type: "+ items.get(position).getType());
        Picasso.get().load(items.get(position).getPoster()).into(holder.imageView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, MovieActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("movieId", items.get(position).getImdbID());
                intent.putExtras(bundle);
                context.startActivity(intent);


            }
        });

    }

    @Override
    public int getItemCount() {
        return items.size();
    }


}
