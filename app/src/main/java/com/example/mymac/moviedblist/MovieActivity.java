package com.example.mymac.moviedblist;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mymac.moviedblist.Model.Movie;
import com.example.mymac.moviedblist.Model.SearchResponse;
import com.example.mymac.moviedblist.Retrofit.ApiClientRetrofit;
import com.example.mymac.moviedblist.Retrofit.ApiInterfaceRetrofit;
import com.squareup.picasso.Picasso;

import retrofit2.Call;
import retrofit2.Response;

public class MovieActivity extends AppCompatActivity {

    String movieId;

    TextView title, release, length, genre,plot;
    ImageView image;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie);
        movieId = getIntent().getExtras().getString("movieId");


        title = findViewById(R.id.movie_title);
        release = findViewById(R.id.movie_release);
        length = findViewById(R.id.movie_length);
        genre = findViewById(R.id.movie_genre);
        plot = findViewById(R.id.movie_plot);
        image = findViewById(R.id.movie_image);

        ApiInterfaceRetrofit retrofit = ApiClientRetrofit.getClient().create(ApiInterfaceRetrofit.class);
        Call<Movie> call;
        call = retrofit.showMovie(movieId);
        call.enqueue(new retrofit2.Callback<Movie>() {
            @Override
            public void onResponse(Call<Movie> call, Response<Movie> response) {
                Log.d("movies", response.body().toString());
                Movie movie = response.body();
                title.setText(movie.getTitle());
                release.setText(movie.getReleased());
                length.setText(movie.getRuntime());
                genre.setText(movie.getGenre());
                plot.setText(movie.getPlot());
                Picasso.get().load(movie.getPoster()).into(image);
            }
            @Override
            public void onFailure(Call<Movie> call, Throwable t) {
                Log.d("movies failure",  t.toString());
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }



}
