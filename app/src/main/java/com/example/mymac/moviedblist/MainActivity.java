package com.example.mymac.moviedblist;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.mymac.moviedblist.Model.SearchResponse;
import com.example.mymac.moviedblist.Retrofit.ApiClientRetrofit;
import com.example.mymac.moviedblist.Retrofit.ApiInterfaceRetrofit;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    static String chosenField = "All";
    EditText searchText;
    RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_main);


        Spinner field= findViewById(R.id.search_spinner);
        searchText = findViewById(R.id.search_text);
        Button searchButton = findViewById(R.id.search_button);
        recyclerView= findViewById(R.id.movie_recycler);


        List<String> categories1 = new ArrayList<String>();
        categories1.add("Type");
        categories1.add("movie");
        categories1.add("series");
        categories1.add("episode");

        final ArrayAdapter<String> fieldAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories1);
        fieldAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        field.setAdapter(fieldAdapter);
        field.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                switch (position){
                    case 0: chosenField= "Type";
                        break;
                    case 1: chosenField= "movie";
                        break;
                    case 2: chosenField= "series";
                        break;
                    case 3: chosenField= "episode";
                        break;
                }
                Log.d("chosenField", chosenField);

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        searchButton.setOnClickListener(v->{
            if(searchText.getText().toString().equals("") ) {
                searchButton.setError("required");
                return;
            }
            search(searchText.getText().toString().trim(),chosenField);
        });
    }

    private void search(String searchWord, String chosenField) {
        ApiInterfaceRetrofit retrofit = ApiClientRetrofit.getClient().create(ApiInterfaceRetrofit.class);
        Call<SearchResponse> call;
        if(!chosenField.equals("Type")) call = retrofit.showListOfMoviesWithType(searchWord,chosenField);
        else call = retrofit.showListOfMovies(searchWord);
        call.enqueue(new retrofit2.Callback<SearchResponse>() {
            @Override
            public void onResponse(Call<SearchResponse> call, Response<SearchResponse> response) {
                Log.d("movies", "hello");
                Log.d("movies", response.body().toString());
                if(response.body().getTotalResults() == 0){
                    Toast.makeText(getApplicationContext(),"nop results",Toast.LENGTH_LONG).show();
                    return;
                }



                recyclerView.setHasFixedSize(true);
                RecyclerView.LayoutManager manager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false);
                recyclerView.setLayoutManager(manager);
                MovieRcyclerAdapter movieRcyclerAdapter = new MovieRcyclerAdapter(response.body().getItems(), getApplicationContext());
                recyclerView.setAdapter(movieRcyclerAdapter);


            }
            @Override
            public void onFailure(Call<SearchResponse> call, Throwable t) {
                Log.d("movies failure",  t.toString());


            }
        });
    }


}
