package com.example.mymac.moviedblist.Retrofit;



import com.example.mymac.moviedblist.Model.Movie;
import com.example.mymac.moviedblist.Model.SearchResponse;

import retrofit2.Call;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface ApiInterfaceRetrofit {

    //login
//    @POST("users/login")
//    Call<ApiResponse> login(@Query("email") String email, @Query("password") String password);



    @POST("?apikey=ac6764a3")
    Call<SearchResponse> showListOfMoviesWithType(@Query("s") String search, @Query("type") String type);
    @POST("?apikey=ac6764a3")
    Call<SearchResponse> showListOfMovies(@Query("s") String search);
    @POST("?apikey=ac6764a3")
    Call<Movie> showMovie(@Query("i") String id);

    //@Query("apikey") String apiKey,


}
