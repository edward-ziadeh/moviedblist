package com.example.mymac.moviedblist.Model;

import java.util.ArrayList;

public class SearchResponse {
    private ArrayList<Item> Search;
    private int totalResults;
    private boolean Response;

    public SearchResponse(ArrayList<Item> search, int totalResults, boolean response) {
        this.Search = search;
        this.totalResults = totalResults;
        Response = response;
    }

    @Override
    public String toString() {
        return "SearchResponse{" +
                "SearchResponse=" + Search +
                ", totalResults=" + totalResults +
                ", Response=" + Response +
                '}';
    }

    public ArrayList<Item> getItems() {
        return Search;
    }

    public void setItems(ArrayList<Item> items) {
        this.Search = items;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public boolean isResponse() {
        return Response;
    }

    public void setResponse(boolean response) {
        Response = response;
    }
}
